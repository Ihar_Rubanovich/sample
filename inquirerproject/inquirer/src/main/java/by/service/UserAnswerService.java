package by.service;

import by.dao.impl.DaoUserAnswer;
import by.model.UserAnswer;

public class UserAnswerService {
	private DaoUserAnswer daoUserAnswer;

	public UserAnswerService() {
		daoUserAnswer = new DaoUserAnswer();
	}

	public void insertUserAnswer(UserAnswer userAnswer) {
		daoUserAnswer.insertUserAnswer(userAnswer);
	}
}
