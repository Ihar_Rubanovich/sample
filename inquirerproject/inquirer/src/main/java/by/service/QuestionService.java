package by.service;

import java.util.List;

import by.dao.impl.DaoQuestion;
import by.model.Question;
import by.model.Test;

public class QuestionService {
	private DaoQuestion daoQuestion;

	public QuestionService() {
		daoQuestion = new DaoQuestion();
	}

	public int getQuestionId(Question question) {
		return daoQuestion.getQuestionID(question);
	}

	public void insertQuestion(Question question, Test test) {
		daoQuestion.insertQuestion(question, test);
	}

	public List<Question> getAllQuestions(Test test) {
		return daoQuestion.getAllQuestions(test);
	}

	public String getQuestionTitle(String questionId) {
		return daoQuestion.getQuestionTitle(questionId);
	}

	public void updateQuestion(Question question) {
		daoQuestion.updateQuestion(question);
	}

	public void deleteQuestion(Question question) {
		daoQuestion.deleteQuestion(question);

	}
}
