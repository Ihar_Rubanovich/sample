package by.service;

import java.util.List;

import by.dao.impl.DaoAnswer;
import by.model.Answer;
import by.model.Question;

public class AnswerService {
	private DaoAnswer daoAnswer;

	public AnswerService() {
		daoAnswer = new DaoAnswer();
	}

	public void insertAnswers(Question question, Answer answer) {
		daoAnswer.insertAnswers(question, answer);
	}

	public List<Answer> getAllAnswers(Question question) {
		return daoAnswer.getAllAnswers(question);
	}

	public void updateAnswer(Answer answer, Question question) {
		daoAnswer.updateAnswer(answer, question);
	}

	public String getTitleAnswer(String answerId) {
		return daoAnswer.getTitleAnswer(answerId);
	}
}
