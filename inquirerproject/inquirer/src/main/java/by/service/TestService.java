package by.service;

import java.util.List;

import by.dao.impl.DaoTest;
import by.model.Test;

public class TestService {
	private DaoTest daoTest;

	public TestService() {
		daoTest = new DaoTest();
	}

	public List<Test> getAllTests() {
		return daoTest.getAllTests();
	}

	public Test selectTest(String idTest) {
		return daoTest.selectTest(idTest);
	}

	public void deleteTest(Test test) {
		daoTest.deleteTest(test);

	}

	public void updateTest(Test test) {
		daoTest.update(test);

	}
	public void addTest(Test test){
		daoTest.addTest(test);
	}
	public int getTestId(Test test){
		return  daoTest.getTestId(test);
	}
	
	
}
