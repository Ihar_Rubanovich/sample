package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Answer;
import by.model.Question;
import by.model.Test;
import by.service.AnswerService;
import by.service.QuestionService;

public class AnswersEditCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		QuestionService questionService = new QuestionService();
		Question question = new Question();

		if (request.getParameterValues("check") != null) {
			String[] id = request.getParameterValues("check");
			for (String quest : id) {
				question.setId(Integer.parseInt(quest));
				questionService.deleteQuestion(question);
			}
			Test test = (Test) request.getSession().getAttribute(Constants.TEST);
			List<Question> questions = questionService.getAllQuestions(test);
			request.setAttribute(Constants.QUESTIONS, questions);
			return Constants.QUESTION_LIST_PAGE;
		}

		AnswerService answerService = new AnswerService();
		question.setTitle(request.getParameter("questionTitle"));
		question.setId(Integer.parseInt(request.getParameter("questionId")));
		List<Answer> answers = answerService.getAllAnswers(question);
		request.setAttribute(Constants.ANSWERS, answers);
		request.setAttribute(Constants.QUESTION, question);
		return Constants.ANSWERS_EDIT_PAGE;
	}
}
