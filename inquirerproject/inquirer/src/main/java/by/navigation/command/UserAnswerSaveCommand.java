package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Answer;
import by.model.Question;
import by.model.Test;
import by.model.User;
import by.model.UserAnswer;
import by.service.AnswerService;
import by.service.QuestionService;
import by.service.UserAnswerService;

public class UserAnswerSaveCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		QuestionService questionService = new QuestionService();
		Question question = new Question();

		if (request.getParameterValues("check") != null) {
			String[] id = request.getParameterValues("check");
			for (String quest : id) {
				question.setId(Integer.parseInt(quest));
				// questionService.deleteQuestion(question);
			}
			Test test = (Test) request.getSession()
					.getAttribute(Constants.TEST);
			List<Question> questions = questionService.getAllQuestions(test);
			request.setAttribute(Constants.QUESTIONS, questions);
			return Constants.USER_TEST_ACTION_PAGE;
		}

		AnswerService answerService = new AnswerService();
		question.setTitle(request.getParameter("questionTitle"));
		question.setId(Integer.parseInt(request.getParameter("questionId")));
		List<Answer> answers = answerService.getAllAnswers(question);
		request.setAttribute(Constants.ANSWERS, answers);
		request.setAttribute(Constants.QUESTION, question);

		UserAnswer answer = new UserAnswer();
		answer.setQuestionId(Integer.parseInt(request
				.getParameter("questionId")));
		// answer.setTestId(Integer.parseInt(request.getParameter("id")));
		request.getSession().getAttribute(Constants.USER);
		User user = new User();

		answer.setUserId(user.getId());
		// answer.setQuestionId(Integer.parseInt(request
		// .getParameter("questionId")));
		// answer.setAnswerId(Integer.parseInt(request.getParameter("answerId")));

		// if (request.getParameter(Constants.ACTION).equals("Save")) {
		// answer.setAnswerId(Integer.parseInt(request
		// .getParameter("answerId")));
		// UserAnswerService userAnswerService = new UserAnswerService();
		// userAnswerService.insertUserAnswer(answer);
		// }
		return Constants.ANSWER_ACTION_PAGE;
	}

}
