package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Answer;
import by.model.Question;
import by.model.Test;
import by.service.AnswerService;
import by.service.QuestionService;

public class AnswersSaveCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {

		QuestionService questionService = new QuestionService();
		Question question = new Question();
		question.setTitle(request.getParameter("questionTitle"));
		question.setId(Integer.parseInt(request.getParameter("questionId")));
		questionService.updateQuestion(question);
		Answer answer = new Answer();
		AnswerService answerService = new AnswerService();

		String[] answersId = request.getParameterValues("answerId");
		String[] answersTitle = request.getParameterValues("answeros");
		for (int i = 0; i < answersId.length; i++) {
			answer.setId(Integer.parseInt(answersId[i]));
			answer.setTitle(answersTitle[i]);
			answerService.updateAnswer(answer, question);
		}
		Test test = (Test) request.getSession().getAttribute(Constants.TEST);
		List<Question> questions = questionService.getAllQuestions(test);
		request.setAttribute(Constants.QUESTIONS, questions);

		return Constants.QUESTION_LIST_PAGE;
	}
}
