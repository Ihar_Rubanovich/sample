package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Test;
import by.service.TestService;

public class Test_LIST_Command implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameterValues("check") != null) {
			String[] id = request.getParameterValues("check");
			TestService testService = new TestService();
			Test test = new Test();
			for (int i = 0; i < id.length; i++) {
				test.setId(Integer.parseInt(id[i]));
				testService.deleteTest(test);
			}
			request.getSession().setAttribute(Constants.TESTDELETE, id);
			List<Test> tests = testService.getAllTests();
			request.setAttribute(Constants.TESTS, tests);
			return Constants.MAIN_ADMIN_PAGE;
		}

		String idTest = request.getParameter("testId");
		TestService testService = new TestService();
		Test test = testService.selectTest(idTest);
		request.getSession().setAttribute(Constants.TEST, test);
		return Constants.TEST_EDIT_PAGE;

	}

}
