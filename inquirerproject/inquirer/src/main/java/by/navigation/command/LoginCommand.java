package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.dao.impl.DaoCheckUser;
import by.model.Test;
import by.model.User;
import by.service.TestService;

public class LoginCommand implements Command {

	public String execute(HttpServletRequest request) {
		DaoCheckUser id = new DaoCheckUser();

		String pass = request.getParameter(Constants.PASS);
		String login = request.getParameter(Constants.LOGIN);

		id.checkUser();
		String userN = id.getUserN();
		String userP = id.getUserP();
		String userRole = id.getUserRole();

		if (userN.equals(login) & userP.equals(pass) & userRole.equals("admin")) {
			TestService service = new TestService();
			List<Test> tests = service.getAllTests();
			request.setAttribute(Constants.TESTS, tests);
			User user = new User();
			user.setLogin(login);
			request.getSession().setAttribute(Constants.USER, user);

			return Constants.MAIN_ADMIN_PAGE;
		} else {
			if (userN.equals(login) & userP.equals(pass)
					& userRole.equals("user")) {
				TestService service = new TestService();
				List<Test> tests = service.getAllTests();
				request.setAttribute(Constants.TESTS, tests);
				User user = new User();
				user.setLogin(login);
				request.getSession().setAttribute(Constants.USER, user);

				return Constants.MAIN_USER_PAGE;
			}

			return Constants.ERROR_LOGIN_PAGE;

		}
	}
}
