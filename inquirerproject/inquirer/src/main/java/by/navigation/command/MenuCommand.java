package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.constants.Constants;
import by.model.Test;
import by.service.TestService;

public class MenuCommand implements Command {

	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (request.getParameter("action").equals(Constants.MENU_LIST_TEST)) {

			if (session != null) {
				session.removeAttribute(Constants.TEST);
			}
			TestService service = new TestService();
			List<Test> tests = service.getAllTests();
			request.setAttribute(Constants.TESTS, tests);
			return Constants.MAIN_ADMIN_PAGE;
		} else {
			if (session != null) {
				session.removeAttribute(Constants.TEST);
			}
			return Constants.TEST_EDIT_PAGE;
		}
	}
}
