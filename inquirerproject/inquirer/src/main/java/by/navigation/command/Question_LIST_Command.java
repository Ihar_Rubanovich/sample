package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Question;
import by.model.Test;
import by.service.QuestionService;
import by.service.TestService;

public class Question_LIST_Command implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter("updateTest").equals("qList")) {
			QuestionService questionService = new QuestionService();
			Test test = (Test) request.getSession()
					.getAttribute(Constants.TEST);
			List<Question> questions = questionService.getAllQuestions(test);
			request.setAttribute(Constants.QUESTIONS, questions);
			return Constants.QUESTION_LIST_PAGE;

		}
		if (request.getParameter("updateTest").equals("tests")) {
			
			TestService service = new TestService();
			List<Test> tests = service.getAllTests();
			request.setAttribute(Constants.TESTS, tests);
			return Constants.MAIN_ADMIN_PAGE;
		} else {
			String testId = request.getParameter("id");
			String testTitle = request.getParameter("title");
			String testDescription = request.getParameter("description");
			Test test = new Test();
			test.setId(Integer.parseInt(testId));
			test.setTitle(testTitle);
			test.setDescription(testDescription);
			TestService testService = new TestService();
			testService.updateTest(test);
			List<Test> tests = testService.getAllTests();
			request.setAttribute(Constants.TESTS, tests);
			return Constants.MAIN_ADMIN_PAGE;
		}
	}
}
