package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Test;
import by.service.TestService;

public class TestCommand implements Command {

	public String execute(final HttpServletRequest request) {
		Test test = new Test();
		String title = request.getParameter(Constants.TITLE);
		String description = request.getParameter(Constants.DESCRIPTION);
		String id = request.getParameter("id");

		TestService testService = new TestService();
		test.setTitle(title);
		test.setDescription(description);

		if (request.getParameter("updateTest") != null) {
			test.setId(Integer.parseInt(id));
			testService.updateTest(test);
			List<Test> tests = testService.getAllTests();
			request.setAttribute(Constants.TESTS, tests);
			return Constants.MAIN_ADMIN_PAGE;
		} else {
			if (request.getParameter("action") != null) {
				testService.addTest(test);
			}
		}
		testService.getTestId(test);
		request.getSession().setAttribute(Constants.TEST, test);
		return Constants.QUESTION_ADD_PAGE;

	}
}