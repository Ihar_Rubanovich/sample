package by.navigation.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.constants.Constants;

public class LogOffCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session != null) {
			session.invalidate();
		}
		return Constants.LOGIN_PAGE;
	}

}
