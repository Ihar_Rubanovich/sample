package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Answer;
import by.model.Question;
import by.model.Test;
import by.service.AnswerService;
import by.service.QuestionService;

public class QuestionCommand implements Command {
	@Override
	public String execute(HttpServletRequest request) {
		Answer answer1 = new Answer();
		Answer answer2 = new Answer();
		Answer answer3 = new Answer();
		Answer answer4 = new Answer();
		Test test = (Test) request.getSession().getAttribute(Constants.TEST);
		Question question = new Question();
		if (request.getParameter(Constants.ACTION).equals("Save")) {
			String title = request.getParameter(Constants.QUESTION_TITLE);
			String status = request.getParameter(Constants.STATUS);
			if (isCheck1(status)) {
				answer1.setStatus(Constants.STATUS_Y);
			} else {
				answer1.setStatus(Constants.STATUS_N);
			}
			if (isCheck2(status)) {
				answer2.setStatus(Constants.STATUS_Y);
			} else {
				answer2.setStatus(Constants.STATUS_N);
			}
			if (isCheck3(status)) {
				answer3.setStatus(Constants.STATUS_Y);
			} else {
				answer3.setStatus(Constants.STATUS_N);
			}
			if (isCheck4(status)) {
				answer4.setStatus(Constants.STATUS_Y);
			} else {
				answer4.setStatus(Constants.STATUS_N);
			}

			question.setTitle(title);

			String qAnswer1 = request.getParameter(Constants.ANSWER1);
			String qAnswer2 = request.getParameter(Constants.ANSWER2);
			String qAnswer3 = request.getParameter(Constants.ANSWER3);
			String qAnswer4 = request.getParameter(Constants.ANSWER4);

			QuestionService questionService = new QuestionService();
			questionService.insertQuestion(question, test);
			questionService.getQuestionId(question);
			request.getSession().setAttribute(Constants.QUESTION_ID,
					question.getId());

			AnswerService answerService = new AnswerService();
			answer1.setTitle(qAnswer1);
			answerService.insertAnswers(question, answer1);
			answer2.setTitle(qAnswer2);
			answerService.insertAnswers(question, answer2);
			answer3.setTitle(qAnswer3);
			answerService.insertAnswers(question, answer3);
			answer4.setTitle(qAnswer4);
			answerService.insertAnswers(question, answer4);

			List<Question> questions = questionService.getAllQuestions(test);
			request.setAttribute(Constants.QUESTIONS, questions);
			return Constants.QUESTION_LIST_PAGE;
		} else {
			return Constants.TEST_EDIT_PAGE;
		}
	}

	private boolean isCheck1(final String status) {
		return status.equals(Constants.ANSWER_CHECK_1);
	}

	private boolean isCheck2(final String status) {
		return status.equals(Constants.ANSWER_CHECK_2);
	}

	private boolean isCheck3(final String status) {
		return status.equals(Constants.ANSWER_CHECK_3);
	}

	private boolean isCheck4(final String status) {
		return status.equals(Constants.ANSWER_CHECK_4);
	}

}
