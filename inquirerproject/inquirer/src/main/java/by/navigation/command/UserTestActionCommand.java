package by.navigation.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.constants.Constants;
import by.model.Answer;
import by.model.Question;
import by.model.Test;
import by.service.AnswerService;
import by.service.QuestionService;
import by.service.TestService;

public class UserTestActionCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {

		String idTest = request.getParameter("testId");
		TestService testService = new TestService();
		Test test = testService.selectTest(idTest);
		request.getSession().setAttribute(Constants.TEST, test);
		Answer answer1 = new Answer();
		Answer answer2 = new Answer();
		Answer answer3 = new Answer();
		Answer answer4 = new Answer();
		Question question = new Question();
		String title = request.getParameter(Constants.QUESTION_TITLE);

		question.setTitle(title);

		String qAnswer1 = request.getParameter(Constants.ANSWER1);
		String qAnswer2 = request.getParameter(Constants.ANSWER2);
		String qAnswer3 = request.getParameter(Constants.ANSWER3);
		String qAnswer4 = request.getParameter(Constants.ANSWER4);

		QuestionService questionService = new QuestionService();
//		questionService.insertQuestion(question, test);
		questionService.getQuestionId(question);
//		questionService.getQuestionTitle(question);
		request.getSession().setAttribute(Constants.QUESTION_ID,
				question.getId());

		AnswerService answerService = new AnswerService();
		answer1.setTitle(qAnswer1);
		answerService.insertAnswers(question, answer1);
		answer2.setTitle(qAnswer2);
		answerService.insertAnswers(question, answer2);
		answer3.setTitle(qAnswer3);
		answerService.insertAnswers(question, answer3);
		answer4.setTitle(qAnswer4);
		answerService.insertAnswers(question, answer4);

		List<Question> questions = questionService.getAllQuestions(test);
		request.setAttribute(Constants.QUESTIONS, questions);

		return Constants.USER_TEST_ACTION_PAGE;
	}
}
