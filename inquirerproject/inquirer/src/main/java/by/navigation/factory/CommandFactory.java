package by.navigation.factory;

import by.navigation.command.AnswersEditCommand;
import by.navigation.command.AnswersSaveCommand;
import by.navigation.command.Command;
import by.navigation.command.LogOffCommand;
import by.navigation.command.LoginCommand;
import by.navigation.command.MenuCommand;
import by.navigation.command.QuestionCommand;
import by.navigation.command.Question_LIST_Command;
import by.navigation.command.TestCommand;
import by.navigation.command.Test_LIST_Command;
import by.navigation.command.UserAnswerSaveCommand;
import by.navigation.command.UserTestActionCommand;

public class CommandFactory {

	public Command getCommand(String paramPage) {
		Commands command = Commands.valueOf(paramPage.toUpperCase());
		switch (command) {
		case START:
			return new UserTestActionCommand();
		case LOGIN_COMMAND:
			return new LoginCommand();
		case TEST:
			return new TestCommand();
		case TEST_LIST:
			return new Test_LIST_Command();
		case TEST_EDIT:
			return new Question_LIST_Command();
		case QUESTION:
			return new QuestionCommand();
		case LISTQUESTIONS:
			return new AnswersEditCommand();
		case ANSWERACTION:
			return new UserAnswerSaveCommand();
		case ANSWER_EDIT:
			return new AnswersSaveCommand();
		case MENU:
			return new MenuCommand();
		case HEADER:
			return new LogOffCommand();
		default:
			throw new IllegalAccessError();
		}
	}
}
