package by.logger.log;

import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

public class Log {

	private static Logger logger;

	public static Logger getLogger() {
		logger = Logger.getLogger(Log.class.getName());
		PropertyConfigurator.configure("log4j.properties");
		logger.setLevel(Level.ALL);
		return logger;
	}
}
