package by.model;

public class Test {
	private int idTest;
	private String title;
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return idTest;
	}

	public void setId(int id) {
		this.idTest = id;
	}

}
