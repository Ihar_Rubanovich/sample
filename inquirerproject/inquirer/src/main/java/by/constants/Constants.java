package by.constants;

public class Constants {
	public static final String TEST_PAGE = "/pages/testAddPage.jsp";
	public static final String MAIN_USER_PAGE = "/pages/mainUserPage.jsp";
	public static final String MAIN_ADMIN_PAGE = "/pages/mainAdminPage.jsp";
	public static final String LOGIN_PAGE = "index.jsp";
	public static final String QUESTION_ADD_PAGE = "/pages/questionAddPage.jsp";
	public static final String ERROR_LOGIN_PAGE = "/pages/errorLoginPage.jsp";
	public static final String TEST_EDIT_PAGE = "/pages/testEditPage.jsp";
	public static final String QUESTION_LIST_PAGE = "/pages/questionsListPage.jsp";
	public static final String ANSWERS_EDIT_PAGE = "/pages/tiles/ANSWERS_EDIT.jsp";
	public static final String USER_TEST_ACTION_PAGE = "/pages/tiles/userTestActionPage.jsp";
	public static final String USERS_QUESTION_LIST = "/pages/tiles/usersQuestionList.jsp";
	public static final String ANSWER_ACTION_PAGE = "/pages/tiles/answerAction.jsp";

	public static final String PARAM_PAGE = "page";
	public static final String LOGIN = "login";
	public static final String PASS = "password";
	public static final String USER = "user";

	public static final String TESTS = "tests";
	public static final String TEST = "test";
	public static final String TEST_ID = "test_id";
	public static final String TITLE = "title";
	public static final String DESCRIPTION = "description";

	public static final String QUESTION_ID = "id";
	public static final String QUESTION_TITLE = "title";
	public static final String QUESTIONS = "questions";
	public static final String QUESTION = "question";

	public static final String MENU_LIST_TEST = "List Test";

	public static final String ANSWER1 = "answer1";
	public static final String ANSWER2 = "answer2";
	public static final String ANSWER3 = "answer3";
	public static final String ANSWER4 = "answer4";
	public static final String ANSWERS = "answers";
	public static final String STATUS = "status";
	public static final String ACTION = "action";
	public static final String START = "start";

	public static final String STATUS_Y = "Y";
	public static final String STATUS_N = "N";
	public static final String ANSWER_CHECK_1 = "check1";
	public static final String ANSWER_CHECK_2 = "check2";
	public static final String ANSWER_CHECK_3 = "check3";
	public static final String ANSWER_CHECK_4 = "check4";
	public static final String TESTDELETE = "TestDelete";

}