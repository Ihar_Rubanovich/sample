package by.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.constants.Constants;
import by.navigation.command.Command;
import by.navigation.factory.CommandFactory;

public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Controller() {

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		performAction(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		performAction(request, response);
	}

	private void performAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String page = null;
		String paramPage = request.getParameter(Constants.PARAM_PAGE);
		if (paramPage != null && !paramPage.isEmpty()) {
			CommandFactory commandFactory = new CommandFactory();
			Command command = commandFactory.getCommand(paramPage);
			page = command.execute(request);
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher(page);
			requestDispatcher.forward(request, response);
		} else {
			throw new IllegalAccessError(
					"Error with access to class from Controller.java");
		}

	}

}
