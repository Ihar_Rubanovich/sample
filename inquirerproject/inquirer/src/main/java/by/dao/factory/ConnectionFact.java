package by.dao.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import by.logger.log.Log;

public class ConnectionFact {
	
	private String driver;
	private String url;
	private String username;
	private String password;
	private static ConnectionFact connectionFact;
	
	private ConnectionFact() {
		try {
			Properties properties=new Properties();
			properties.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("config.properties"));
			url=properties.getProperty("url");
			driver=properties.getProperty("driver");
			username=properties.getProperty("username");
			password=properties.getProperty("password");
			Class.forName(driver);
		} catch (Exception e) {
			Log.getLogger().info("Can't connect to database: error with ConnectionFactory");
		}
	}

	public Connection getConnection() throws SQLException {
		Connection con = null;
		con = DriverManager.getConnection(url, username, password);
		return con;
	}

	public static ConnectionFact getInstance() {
		if (connectionFact == null) {
			connectionFact = new ConnectionFact();
		}
		return connectionFact;
	}
}