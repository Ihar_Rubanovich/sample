package by.dao.interfaces;

import by.model.Test;
import java.util.List;

public interface IDaoTest {

	public void addTest(Test test);

	public void deleteTest(Test test);

	public void update(Test test);

	public int getTestId(Test test);

	public List<Test> getAllTests();

}
