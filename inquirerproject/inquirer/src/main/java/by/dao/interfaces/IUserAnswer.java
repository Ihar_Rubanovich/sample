package by.dao.interfaces;

import java.util.List;

import by.model.UserAnswer;

public interface IUserAnswer {

	public void insertUserAnswer(UserAnswer userAnswer);

	public List<UserAnswer> getAllUserAnswers(UserAnswer userAnswer);

}
