package by.dao.interfaces;

import java.util.List;

import by.model.Question;
import by.model.Test;

public interface IQuestion {

	public void insertQuestion(Question question, Test test);

	public void updateQuestion(Question question);

	public int getQuestionID(Question question);

	public List<Question> getAllQuestions(Test test);
}
