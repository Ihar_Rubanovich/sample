package by.dao.interfaces;

import java.util.List;

import by.model.Answer;
import by.model.Question;

public interface IAnswer {
	public void insertAnswers(Question question, Answer answers);

	public List<Answer> getAllAnswers(Question question);

	public void updateAnswer(Answer answers, Question question);

}
