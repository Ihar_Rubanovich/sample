package by.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import by.dao.factory.ConnectionFact;
import by.dao.interfaces.IUserAnswer;
import by.logger.log.Log;
import by.model.UserAnswer;

public class DaoUserAnswer implements IUserAnswer {

	Connection con;
	PreparedStatement ptmt;
	ResultSet resultSet;

	public DaoUserAnswer() {

	}

	private Connection getConnection() throws SQLException {
		return ConnectionFact.getInstance().getConnection();
	}

	@Override
	public void insertUserAnswer(UserAnswer userAnswer) {
		try {
			String queryString = "INSERT INTO useranswer(user_id,test_id,question_id,status) VALUES(?,?)";
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ptmt.setInt(1, userAnswer.getUserId());
			ptmt.setInt(2, userAnswer.getAnswerId());
//			ptmt.setInt(3, userAnswer.getQuestionId());
//			ptmt.setString(2, userAnswer.getStatus());

			ptmt.executeUpdate();
		} catch (SQLException e) {
			Log.getLogger().info("insert user answer error");
			e.printStackTrace();
		} finally {
			try {
				if (ptmt != null)
					ptmt.close();
				if (con != null)
					con.close();
			} catch (SQLException e) {
				Log.getLogger()
						.info("insert on close connection or ptmt error");
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<UserAnswer> getAllUserAnswers(UserAnswer userAnswer) {
		return null;
	}

}
