package by.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.dao.factory.ConnectionFact;
import by.dao.interfaces.IDaoTest;
import by.logger.log.Log;
import by.model.Test;

public class DaoTest implements IDaoTest {

	Connection con;
	PreparedStatement ptmt;
	ResultSet resultSet;

	public DaoTest() {
	}

	private Connection getConnection() throws SQLException {
		return ConnectionFact.getInstance().getConnection();
	}

	public void addTest(Test test) {
		try {
			String queryString = "INSERT INTO tests(title,description) VALUES(?,?)";
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ptmt.setString(1, test.getTitle());
			ptmt.setString(2, test.getDescription());
			ptmt.executeUpdate();
		} catch (SQLException e) {
			Log.getLogger().info("insert Test error");
			e.printStackTrace();
		} finally {
			try {
				if (ptmt != null)
					ptmt.close();
				if (con != null)
					con.close();
			} catch (SQLException e) {
				Log.getLogger()
						.info("insert on close connection or ptmt error");
				e.printStackTrace();
			}
		}
	}

	public void update(Test test) {
		try {
			String queryString = "UPDATE tests SET title=?,description=? WHERE tests.id=?";
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ptmt.setString(1, test.getTitle());
			ptmt.setString(2, test.getDescription());
			ptmt.setInt(3, test.getId());
			ptmt.executeUpdate();
		} catch (SQLException e) {
			Log.getLogger().info("insert updateTest error");
			e.printStackTrace();
		} finally {
			try {
				if (ptmt != null)
					ptmt.close();
				if (con != null)
					con.close();
			} catch (SQLException e) {
				Log.getLogger().info("insert updateTest error");
				e.printStackTrace();
			}
		}
	}

	public List<Test> getAllTests() {

		List<Test> tests = new ArrayList<Test>();
		try {
			String queryString = "SELECT  * FROM tests";
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ResultSet rs = ptmt.executeQuery();
			while (rs.next()) {
				Test test = new Test();
				test.setId(rs.getInt("id"));
				test.setTitle(rs.getString("title"));
				test.setDescription(rs.getString("description"));
				tests.add(test);
			}
		} catch (SQLException e) {
			Log.getLogger().info("insert getAllTests error");
			e.printStackTrace();
		}
		return tests;
	}

	public void deleteTest(Test test) {
		try {
			String queryString = "DELETE FROM tests WHERE tests.id=?";
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ptmt.setInt(1, test.getId());
			ptmt.executeUpdate();

		} catch (SQLException e) {
			Log.getLogger().info("insert deleteTest error");
			e.printStackTrace();
		}
	}

	@Override
	public int getTestId(Test test) {
		String selectTestId = "SELECT id FROM tests where title='"
				+ test.getTitle() + "'";
		try {
			con = getConnection();
			ptmt = con.prepareStatement(selectTestId);
			resultSet = ptmt.executeQuery();
			while (resultSet.next()) {
				test.setId(resultSet.getInt("id"));
			}

		} catch (SQLException e) {
			Log.getLogger().info("insert getTestId error");
			e.printStackTrace();
		}
		return test.getId();

	}

	public Test selectTest(String idTest) {
		String queryString = "SELECT  * FROM tests WHERE id='" + idTest + "'";
		Test test = new Test();
		try {
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ResultSet rs = ptmt.executeQuery();
			while (rs.next()) {
				test.setId(rs.getInt("id"));
				test.setTitle(rs.getString("title"));
				test.setDescription(rs.getString("description"));
			}
		} catch (SQLException e) {
			Log.getLogger().info("insert deleteTest error");
			e.printStackTrace();
		}
		return test;
	}
}
