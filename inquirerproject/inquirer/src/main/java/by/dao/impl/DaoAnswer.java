package by.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.dao.factory.ConnectionFact;
import by.dao.interfaces.IAnswer;
import by.logger.log.Log;
import by.model.Answer;
import by.model.Question;

public class DaoAnswer implements IAnswer {

	private Connection con;
	private PreparedStatement ptmt;

	public DaoAnswer() {
	}

	private Connection getConnection() throws SQLException {
		return ConnectionFact.getInstance().getConnection();
	}

	@Override
	public void insertAnswers(Question question, Answer answer) {

		try {

			String queryString = "INSERT INTO "
					+ "answers(question_id,title,status) VALUES (?,?,?) ";
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ptmt.setInt(1, question.getId());
			ptmt.setString(2, answer.getTitle());
			ptmt.setString(3, answer.getStatus());
			ptmt.executeUpdate();
		} catch (SQLException e) {
			Log.getLogger().info("insertAnswers error");
			e.printStackTrace();

		} finally {
			try {
				if (ptmt != null) {
					ptmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				Log.getLogger().info(
						"Error on close PrepareStatement or Connection");
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Answer> getAllAnswers(Question question) {
		String getAnswers = "SELECT * FROM answers WHERE question_id='"
				+ question.getId() + "'";
		List<Answer> answers = new ArrayList<Answer>();
		try {
			con = getConnection();
			ptmt = con.prepareStatement(getAnswers);
			ResultSet resultSet = ptmt.executeQuery();
			while (resultSet.next()) {
				Answer answer = new Answer();
				answer.setTitle(resultSet.getString("title"));
				answer.setId(resultSet.getInt("id"));
				answer.setStatus(resultSet.getString("status"));
				answers.add(answer);
			}
		} catch (SQLException e) {
			Log.getLogger().info("Error in obtaining list of questions");
			e.printStackTrace();
		}

		return answers;
	}

	@Override
	public void updateAnswer(Answer answer, Question question) {
		String query = "UPDATE answers SET title='" + answer.getTitle()
				+ "' WHERE " + "id='" + answer.getId() + "'";
		try {
			con = getConnection();
			ptmt = con.prepareStatement(query);
			ptmt.executeUpdate();
		} catch (SQLException e) {
			Log.getLogger().info("Error in updateAnswers/DaoAnswers");
			e.printStackTrace();
		}

	}

	public String getTitleAnswer(String answerId) {
		String query = "SELECT * FROM answers WHERE id=?";
		try {
			con = getConnection();
			ptmt = con.prepareStatement(query);
			ptmt.setString(1, answerId);
			ptmt.executeUpdate();
			ResultSet resultSet = ptmt.executeQuery();
			while (resultSet.next()) {
				Answer answer = new Answer();
				answer.setTitle(resultSet.getString("id"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
