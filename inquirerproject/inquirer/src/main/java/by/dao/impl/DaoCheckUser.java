package by.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import by.dao.factory.ConnectionFact;
import by.dao.interfaces.ICheckUser;
import by.logger.log.Log;

public class DaoCheckUser implements ICheckUser {
	private String userN = new String();
	private String userP = new String();
	private String userRole = new String();
	private String userId = new String();

	@Override
	public void checkUser() {
		try {
			String query = "SELECT * FROM users JOIN role ON users.role_id = role.id where role = 'admin' "; // where
																											// role
																											// =
																											// 'user'
			Connection connection = ConnectionFact.getInstance()
					.getConnection();
			PreparedStatement ptmt = connection.prepareStatement(query);
			ResultSet resultSet = ptmt.executeQuery();
			while (resultSet.next()) {
				userN = resultSet.getString("login");
				userP = resultSet.getString("password");
				userRole = resultSet.getString("role");
				userId = resultSet.getString("id");

			}
		} catch (Exception e) {
			Log.getLogger().info("checkUser() method error");
		}
	}

	public String getUserN() {
		return userN;
	}

	public void setUserN(String userN) {
		this.userN = userN;
	}

	public String getUserP() {
		return userP;
	}

	public void setUserP(String userP) {
		this.userP = userP;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
