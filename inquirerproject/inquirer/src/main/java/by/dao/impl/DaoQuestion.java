package by.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.dao.factory.ConnectionFact;
import by.dao.interfaces.IQuestion;
import by.logger.log.Log;
import by.model.Question;
import by.model.Test;

public class DaoQuestion implements IQuestion {
	private Connection con;
	private PreparedStatement ptmt;
	private ResultSet resultSet;

	public DaoQuestion() {
	}

	private Connection getConnection() throws SQLException {
		return ConnectionFact.getInstance().getConnection();
	}

	@Override
	public void insertQuestion(Question question, Test test) {
		try {
			String queryString = "INSERT INTO "
					+ "questions(test_id,title) VALUES (?,?) ";
			con = getConnection();
			ptmt = con.prepareStatement(queryString);
			ptmt.setInt(1, test.getId());
			ptmt.setString(2, question.getTitle());
			ptmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			Log.getLogger().info("insertQuestion error");
		} finally {
			try {
				if (ptmt != null) {
					ptmt.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
				Log.getLogger().info("insertQuestion error");
			}
		}
	}

	@Override
	public int getQuestionID(Question question) {
		String selectID = "SELECT id FROM questions WHERE title='"
				+ question.getTitle() + "'";
		try {
			con = getConnection();
			ptmt = con.prepareStatement(selectID);
			resultSet = ptmt.executeQuery();
			while (resultSet.next()) {
				question.setId(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Log.getLogger().info("insert getQuestion error");
		}
		return question.getId();
	}

	@Override
	public List<Question> getAllQuestions(Test test) {
		String getQuestions = "SELECT * FROM questions WHERE test_id='"
				+ test.getId() + "'";
		List<Question> questions = new ArrayList<Question>();
		try {
			con = getConnection();
			ptmt = con.prepareStatement(getQuestions);
			resultSet = ptmt.executeQuery();
			while (resultSet.next()) {
				Question question = new Question();
				question.setTitle(resultSet.getString("title"));
				question.setId(resultSet.getInt("id"));
				questions.add(question);
			}
		} catch (SQLException e) {
			Log.getLogger().info("Error in obtaining list of questions");
			e.printStackTrace();
		}

		return questions;
	}

	public String getQuestionTitle(String questionId) {
		String getTitle = "SELECT * FROM questions WHERE title='" + questionId
				+ "'";
		String question = null;
		try {
			con = getConnection();
			ptmt = con.prepareStatement(getTitle);
			resultSet = ptmt.executeQuery();

			while (resultSet.next()) {
				question = resultSet.getString("title");
			}
		} catch (SQLException e) {
			Log.getLogger().info("Error in obtaining list of questions");
			e.printStackTrace();
		}

		return question;
	}

	@Override
	public void updateQuestion(Question question) {
		String query = "UPDATE questions SET title='" + question.getTitle()
				+ "' WHERE " + "id='" + question.getId() + "'";
		try {
			con = getConnection();
			ptmt = con.prepareStatement(query);
			ptmt.executeUpdate();
		} catch (SQLException e) {
			Log.getLogger().info("Error in updateQuestion/DaoQuestion");
			e.printStackTrace();
		}

	}

	public void deleteQuestion(Question question) {
		String query = "DELETE FROM questions WHERE questions.id=?";
		try {
			con = getConnection();
			ptmt = con.prepareStatement(query);
			ptmt.setInt(1, question.getId());
			ptmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
