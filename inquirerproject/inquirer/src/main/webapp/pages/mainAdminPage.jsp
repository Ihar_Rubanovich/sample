<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Main Page</title>
	<link rel="stylesheet" type="text/css" href="style/style.css" >
</head>
<body>
	<form action="./Controller" method="post">
		<div class="adminPage">
			<div class="t1"><jsp:include page="tiles/header.jsp">
					<jsp:param value="header" name="header" />
				</jsp:include>
			</div>
			<div class="t2"><jsp:include page="tiles/menu.jsp">
					<jsp:param value="adminMenu" name="menu" />
				</jsp:include>
			</div>
			<div class="t3"><jsp:include page="tiles/listTest.jsp">
					<jsp:param value="test" name="test" />
				</jsp:include>
			</div>
		</div>
	</form>
</body>
</html>