<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Answer the question</title>
	<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
	<form action="Controller" method="get">
		<input type="hidden" name="page" value="answer_action" />
		<div class="question">
				<div class="title">
				<h2>-=Question title=-</h2>
			
				<textarea rows="5" cols="60" name="questionTitle" id="title">${question.title}</textarea>
				<input type="hidden" name="questionId" value="${question.id}">
				</div>
			
			<c:forEach var="answer" items="${answers}">
					<div class="radio">
						<c:choose>
  							<c:when test="${(answer.status)=='Y'}"><input type="radio" name="status" value="${answer.id}" checked="checked" ></c:when>
   							<c:when test="${(answer.status)=='N'}"><input type="radio" name="status" value="${answer.id}" checked="checked" ></c:when>
						</c:choose>
					</div>
					<div class="answer">
						<textarea rows="3" cols="60" name="answeros" id="answer">${answer.title}</textarea>
						<input type="hidden" name="answerId" value="${answer.id}">
					</div>
			</c:forEach>
			<div class="cancelBut">
				<a href="Controller?page=listquestions&amp;questionTitle=${question.title}&amp;questionId=${question.id}">Cancel</a>
			</div>
			<div class="saveToBut">
				<input type="submit" name="save" value="Save">
				</div>
			
		</div>
	</form>
</body>
</html>