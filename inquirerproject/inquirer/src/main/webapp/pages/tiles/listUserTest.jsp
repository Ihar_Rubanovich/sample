<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>List Tests</title>
	<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
<fmt:setLocale value="en"/>
	<fmt:setBundle basename="by.messages.bundle.MessageResources" />
	<fmt:bundle basename="by.messages.bundle.MessageResources" />
	<fieldset>	
		<legend>
			<b>List Tests</b>
		</legend>
		<form action="Controller" method="get">
			<input type="hidden" name="page" value="start">
			<table  border="1" width="100%" cellspacing="0" >
				<tr>
					<th align="center"><fmt:message key="jsp.mainTests.Name_of_the_test" /></th>
					<th align="center"><fmt:message key="jsp.mainTests.description" /></th>
					<th align="center"><fmt:message key="jsp.mainTests.edit" /></th>
				</tr>
				<c:forEach var="test" items="${tests}">
					<tr>
						<td align="center" width="200">${test.title}</td>
						<td align="center" width="200">${test.description}</td>
						<td align="center" width="35">
							<a href="Controller?page=start&amp;testId=${test.id}">start</a>
							<input type="radio" name="testId" value="${test.id}">
						</td>
					</tr>
				</c:forEach>
			</table>
			
<!-- 			<input type="submit" name="action" onclick="return(confirm('Вы уверены?'))"  -->
<!-- 					value="Delete" align="right"> -->
		</form>
	</fieldset>
</body>
</html>