<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Edit QUESTION</title>
	<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
	<form action="Controller" method="post">
		<input type="hidden" name="page" value="question" />
		<div class="question">
			<div class="title">
				<h2>Question title</h2>
				<textarea rows="5" cols="60" name="title" id="title"></textarea>
			</div>
			<div class="radio">
				<input type="radio" name="status" value="check1" checked="checked">
			</div>
			<div class="answer">
				<textarea rows="3" cols="60" name="answer1" id="answer1"></textarea>
			</div>
			<div class="radio">
				<input type="radio" name="status" value="check2">
			</div>

			<div class="answer">
				<textarea rows="3" cols="60" name="answer2" id="answer2"></textarea>
			</div>
			<div class="radio">
				<input type="radio" name="status" value="check3">
			</div>
			<div class="answer">
				<textarea rows="3" cols="60" name="answer3" id="answer3"></textarea>
			</div>
			<div class="radio">
				<input type="radio" name="status" value="check4">
			</div>
			<div class="answer">
				<textarea rows="3" cols="60" name="answer4" id="answer4"></textarea> <p> </p>
			</div>
			<div class="saveToBut">
				<input type="submit" name="action" value="Save">
			</div>
			<div class="cancelBut">
				<input type="submit" name="action" value="Cansel">
			</div>
		</div>
	</form>
</body>
</html>