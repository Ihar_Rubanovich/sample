<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test action</title>
<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
userTestActionPage
	<form name="test" action="./Controller" method="get">
		<input type="hidden" name="page" value="test" />
		<fieldset>
			<legend>
				<b>List of tests</b>
			</legend>

			<div>
				<h2>Title</h2>
			</div>
			<p>
				<textarea rows="5" cols="60" name="title" id="title">${test.title}</textarea>
				<input type="hidden" name="testId" value="${test.id}">
			</p>
			<div>
				<h2>Description</h2>
			</div>
			<p>
				<textarea rows="5" cols="60" name="description" id="description">${test.description} </textarea>
			</p>
			<div id="side-bar">
				<c:choose>
					<c:when test="${test == null}">		
						<p>
							<input type="submit" name="action" value="Add" /> 
							<a href="Controller?page=menu&amp;action=List Test"> Cancel</a>
					   </p>
						</c:when>
						<c:when test="${test != null}">
						<p>
    						 <input type="submit" name="updateTest" value="Answer">
    						 <a href="Controller?page=answeraction&amp;testId=${test.id}&amp;updateTest=qList">Questions list</a> 
       						 <a href="Controller?page=answeraction&amp;updateTest=tests"> Cancel</a>
       					</p>
       					</c:when>
       				</c:choose>
       			</div>
       			<div class="t3"><jsp:include page="usersQuestionList.jsp">
					<jsp:param value="questionList" name="questionList" />
				</jsp:include>
			</div>
		</fieldset>
	</form>

</body>
</html>