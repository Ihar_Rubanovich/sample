<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"></link>
	</head>
<body>
	<fmt:setLocale value="en"/>
	<fmt:setBundle basename="by.messages.bundle.MessageResources" />
	<fmt:bundle basename="by.messages.bundle.MessageResources" />
	
	<div align="right">
		<div id="header">
			<div id="image">
				<img src="pages/img/javawarm_100.jpg" alt="" />
			</div>
			<div align="center">
				<h1><fmt:message key="jsp.header.header" /><br/></h1>
			</div>
			<div align="center">
				<h2 id="h2">Hello '${user.login}'!</h2>
			</div>
			<div align="right">
				<form name="loginForm" method="get" action="Controller">
					<input type="hidden" name="page" value="header">
					<input type="submit" name="action" value="LogOFF">
				</form>
			</div>
		</div>
	</div>
</body>
</html>