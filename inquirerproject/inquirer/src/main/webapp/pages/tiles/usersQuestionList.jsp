<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>question list</title>
	<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
	<fieldset>
		<legend>
			<b>List users questions (useersQuestions)</b>
		</legend>
		<form action="Controller" method="get">
			<input name="page" value="listQuestions" type="hidden">
			<table border="1" width="100%" cellspacing="0">
				<tr>
					<th align="center">Name</th>
					<th align="center">Answer</th>
				</tr>
				<c:forEach var="question" items="${questions}">
					<tr>
						<td align="center" width="400">${question.title}</td>
						<td align="center" width="100">
							<a href="Controller?page=answeraction&amp;questionId=${question.id}&amp;questionTitle=${question.title}">Answer</a>
<%-- 							<input type="radio" name="check" value="${question.id}"> --%>
						</td>
					</tr>
				</c:forEach>
			</table>
					</form>
	</fieldset>
</body>
</html>