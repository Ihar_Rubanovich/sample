<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Inquirer Javakava</title>
<link rel="stylesheet" type="text/css" href="style/style.css" />
</head>
<body>
	user page
	<form action="Controller" method="post">
		<div class="userPage">
			<div class="t1"><jsp:include page="tiles/header.jsp">
					<jsp:param value="header" name="header" />
				</jsp:include>
			</div>
			<div class="t2"><jsp:include page="tiles/menuUser.jsp">
					<jsp:param value="adminMenu" name="menu" />
				</jsp:include>
			</div>
			<div class="t3"><jsp:include page="tiles/listUserTest.jsp">
					<jsp:param value="test" name="test" />
				</jsp:include>
			</div>
		</div>

	</form>
</body>
</html>