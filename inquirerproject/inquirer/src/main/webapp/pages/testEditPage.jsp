<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="style/style.css" />
</head>
<body>
	<form action="./Controller" method="get">
		<div class="addTest" >
			<div class="t1"><jsp:include page="tiles/header.jsp">
					<jsp:param value="header" name="header" />
				</jsp:include>
			</div>
			<div class="t2"><jsp:include page="tiles/menu.jsp">
					<jsp:param value="adminMenu" name="menu" />
				</jsp:include>
			</div>
			<div class="t3"><jsp:include page="tiles/TEST-EDIT.jsp">
					<jsp:param value="test" name="test" />
				</jsp:include>
			</div>
		</div>
	</form>
</body>
</html>