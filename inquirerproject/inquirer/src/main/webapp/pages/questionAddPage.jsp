<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Question Add Page</title>
	<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
	<form action="./Controller" method="post">
		
		<div class="questAppear">
			<div class="t1">
				<jsp:include page="tiles/header.jsp"></jsp:include>
			</div>
			<div class="t2">
				<jsp:include page="tiles/menu.jsp"></jsp:include>
			</div>
			<div class="t3">
				<jsp:include page="tiles/question.jsp"></jsp:include>
			</div>
		</div>
	</form>
</body>
</html>