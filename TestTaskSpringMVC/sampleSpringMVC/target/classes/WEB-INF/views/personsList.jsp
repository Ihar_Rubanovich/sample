<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>All Persons</title>
<jsp:include page="includes.jsp"></jsp:include>
</head>

<body>
	<h1>List Persons</h1>
	<h3>
		<a href="add.html">Add More Persons</a>
	</h3>
	<div id="tableContainer" class="tableContainer">
		<table id="table1" border="1" cellpadding="0" cellspacing="0"
			width="100%"
			style="border: 1px solid black; border-collapse: collapse;"
			class="scrollTable">
			<thead class="fixedHeader">
				<tr class="alternateRow">

					<th>Person's ID</th>
					<th>Person's Name</th>
					<th>Person's Age</th>
					<th>Person's Weight</th>
					<th>Person's Address</th>
				</tr>
			</thead>
			<tbody class="scrollContent">
				<c:forEach items="${persons}" var="person">
					<tr>
						<td><c:out value="${person.id}" /></td>
						<td><c:out value="${person.name}" /></td>
						<td><c:out value="${person.age}" /></td>
						<td><c:out value="${person.weight}" /></td>
						<td><c:out value="${person.address}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div>
		<br>
	</div>
</body>
</html>