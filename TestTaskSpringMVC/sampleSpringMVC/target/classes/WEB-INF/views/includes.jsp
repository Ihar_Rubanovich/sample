<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>includes</title>

<!-- style.css for table design and header actions  -->

<link rel="stylesheet" type="text/css" href="resources/css/style.css">

<!-- connect to  jquery-1.9.1 -->

<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.9.1.min.js"></script>

<!-- calling   header's script -->

<script type="text/javascript"
	src="resources/js/jquery.waterwheelCarousel.js"></script>

<script type="text/javascript" src="resources/js/waterwheelCarousel.js"></script>

<!-- Script for tracing selected Rows  -->
<script language="javascript" src="resources/js/selectRow.js"></script>

<!-- Script for check fields. Send message if field does't filled  -->

<script language="javascript"
	src="resources/js/isEmptyFieldValidation.js"></script>

<!-- In fields with numbers must be entered only digits.  -->

<script language="javascript" src="resources/js/isNumberKey.js"></script>

</head>

<!-- Dynamic header for all pages -->

<div id="carousel">
	<a href="#"><img src="resources/img/1.jpg" id="item-1" /></a> <a
		href="#"><img src="resources/img/2.jpg" id="item-2" /></a> <a
		href="#"><img src="resources/img/3.jpg" id="item-3" /></a>
</div>

<body>
</body>
</html>