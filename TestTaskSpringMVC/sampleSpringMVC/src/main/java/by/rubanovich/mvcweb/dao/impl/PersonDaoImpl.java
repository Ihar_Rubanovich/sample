package by.rubanovich.mvcweb.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import by.rubanovich.mvcweb.dao.PersonDao;
import by.rubanovich.mvcweb.model.Person;

@Repository("personDao")
public class PersonDaoImpl implements PersonDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void addPerson(Person person) {
		sessionFactory.getCurrentSession().saveOrUpdate(person);
	}

	@SuppressWarnings("unchecked")
	public List<Person> listPersons() {
		return (List<Person>) sessionFactory.getCurrentSession().createCriteria(Person.class).list();
	}

	public Person getPerson(int personid) {
		return (Person) sessionFactory.getCurrentSession().get(Person.class, personid);
	}

	public void deletePerson(Person person) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM Person WHERE personid = "+person.getPersonId()).executeUpdate();
	}

}
