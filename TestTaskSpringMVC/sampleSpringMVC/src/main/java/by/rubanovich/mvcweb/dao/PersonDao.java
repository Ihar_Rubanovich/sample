package by.rubanovich.mvcweb.dao;

import java.util.List;

import by.rubanovich.mvcweb.model.Person;
public interface PersonDao {
	
	public void addPerson(Person person);

	public List<Person> listPersons();
	
	public Person getPerson(int personid);
	
	public void deletePerson(Person person);
}
