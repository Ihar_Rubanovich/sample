package by.rubanovich.mvcweb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.rubanovich.mvcweb.dao.PersonDao;
import by.rubanovich.mvcweb.model.Person;
import by.rubanovich.mvcweb.service.PersonService;

@Service("personService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonDao personDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addPerson(Person person) {
		personDao.addPerson(person);
	}
	
	public List<Person> listPersons() {
		return personDao.listPersons();
	}

	public Person getPerson(int personid) {
		return personDao.getPerson(personid);
	}
	
	public void deletePerson(Person person) {
		personDao.deletePerson(person);
	}

}
