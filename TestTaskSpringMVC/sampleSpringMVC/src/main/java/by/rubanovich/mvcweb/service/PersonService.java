package by.rubanovich.mvcweb.service;

import java.util.List;

import by.rubanovich.mvcweb.model.Person;

public interface PersonService {
	
	public void addPerson(Person person);

	public List<Person> listPersons();
	
	public Person getPerson(int personid);
	
	public void deletePerson(Person person);
}
