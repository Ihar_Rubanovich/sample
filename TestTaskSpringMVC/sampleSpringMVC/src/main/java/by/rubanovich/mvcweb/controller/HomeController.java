package by.rubanovich.mvcweb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import by.rubanovich.mvcweb.bean.PersonBean;
import by.rubanovich.mvcweb.model.Person;
import by.rubanovich.mvcweb.service.PersonService;

@Controller
public class HomeController {

	@Autowired
	private PersonService personService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView savePerson(
			@ModelAttribute("command") PersonBean personBean,
			BindingResult result) {
		if ("/save" != null) {
		Person person = prepareModel(personBean);
		personService.addPerson(person);
		}
		return new ModelAndView("redirect:/add.html");
		
	}

	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	public ModelAndView listPersons() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("persons", prepareListofBean(personService.listPersons()));
		return new ModelAndView("personsList", model);
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	
	public ModelAndView addPerson(
			
			@ModelAttribute("command") PersonBean personBean,
			BindingResult result) {
		{
			
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("persons", prepareListofBean(personService.listPersons()));
		
		return new ModelAndView("addPerson", model);
		}

	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("index");
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView editPerson(
			@ModelAttribute("command") PersonBean personBean,
			BindingResult result) {
		personService.deletePerson(prepareModel(personBean));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("person", null);
		model.put("persons", prepareListofBean(personService.listPersons()));
		return new ModelAndView("addPerson", model);
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView deletePerson(
			@ModelAttribute("command") PersonBean personBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("person",
				preparePersonBean(personService.getPerson(personBean.getId())));
		model.put("persons", prepareListofBean(personService.listPersons()));
		return new ModelAndView("addPerson", model);
	}

	private Person prepareModel(PersonBean personBean) {
		Person person = new Person();
		person.setPersonAddress(personBean.getAddress());
		person.setPersonAge(personBean.getAge());
		person.setPersonName(personBean.getName());
		person.setWeight(personBean.getWeight());
		person.setPersonId(personBean.getId());
		personBean.setId(null);
		return person;
	}

	private List<PersonBean> prepareListofBean(List<Person> persons) {
		List<PersonBean> beans = null;
		if (persons != null && !persons.isEmpty()) {
			beans = new ArrayList<PersonBean>();
			PersonBean bean = null;
			for (Person person : persons) {
				bean = new PersonBean();
				bean.setName(person.getPersonName());
				bean.setId(person.getPersonId());
				bean.setAddress(person.getPersonAddress());
				bean.setWeight(person.getWeight());
				bean.setAge(person.getPersonAge());
				beans.add(bean);
			}
		}
		return beans;
	}

	private PersonBean preparePersonBean(Person person) {
		PersonBean bean = new PersonBean();
		bean.setAddress(person.getPersonAddress());
		bean.setAge(person.getPersonAge());
		bean.setName(person.getPersonName());
		bean.setWeight(person.getWeight());
		bean.setId(person.getPersonId());
		return bean;
	}
}
