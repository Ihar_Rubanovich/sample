<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Spring MVC Form</title>

<jsp:include page="includes.jsp"></jsp:include>

</head>
<body>

	<form:form name="form" method="POST" action="save.html"
		onsubmit="return validate()">

		<c:if test="${!empty persons}">

			<h2>List of Persons</h2>

			<div id="tableContainer" class="tableContainer">
				<table id="table1" border="1" cellpadding="0" cellspacing="0"
					width="100%"
					style="border: 1px solid black; border-collapse: collapse;"
					class="scrollTable">
					<thead class="fixedHeader">
						<tr class="alternateRow">
							<th>Person's Name</th>
							<th>Person's Age</th>
							<th>Actions on Row</th>
						</tr>
					</thead>
					<tbody class="scrollContent">
						<c:forEach items="${persons}" var="person">
							<tr class="normalRow">
								<td onclick="h(this)"><c:out value="${person.name}" /></td>
								<td onclick="h(this)"><c:out value="${person.age}" /></td>
								<td onclick="h(this)" align="center"><a
									href="edit.html?id=${person.id}">Edit</a> | <a
									href="delete.html?id=${person.id}">Delete</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div>
				<br>
			</div>
		</c:if>

		<h2>Add or edit person's data</h2>
		<table align="left" border="1">
			<tr>
				<th>Person's ID</th>
				<th>Person's Name</th>
				<th>Person's Age</th>
				<th>Person's Weight</th>
				<th>Person's Address</th>
				<th>Actions on Row</th>
			</tr>
			<tr>

				<td><form:input path="id" value="${person.id}" readonly="true" /></td>

				<td><form:input name="name" path="name" value="${person.name}" /></td>

				<td><form:input type="number" name="age" path="age"
						value="${person.age}" onkeypress="return isNumberKey(event)" /></td>

				<td><form:input type="number" name="weight" path="weight"
						value="${person.weight}" onkeypress="return isNumberKey(event)" /></td>

				<td><form:input name="adress" path="address"
						value="${person.address}" /></td>

				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>